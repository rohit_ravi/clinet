import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/header/header.component';
import { FooterComponent } from './shared/footer/footer.component';
import { ToasterComponent } from './shared/toaster/toaster.component';
import { LoaderComponent } from './shared/loader/loader.component';
import { TableComponent } from './shared/table/table.component';
import { PaginationComponent } from './shared/pagination/pagination.component';
import { LoginComponent } from './shared/login/login.component';
import{ FormsModule, ReactiveFormsModule } from '@angular/forms'
import { ServiceService } from './shared/services/service.service';
import {HttpClientModule} from '@angular/common/http';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,

    LoaderComponent,
    TableComponent,
    PaginationComponent,
    LoginComponent, ToasterComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
   

  
    
  ],
  providers: [ ServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
