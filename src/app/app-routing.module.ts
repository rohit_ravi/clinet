import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './shared/login/login.component';


const routes: Routes = [
  {path:'Login',component:LoginComponent},
  {path:'admin',loadChildren:'./admin/admin.module#AdminModule'},
  {path:'customer',loadChildren:'./customer/customer.module#CustomerModule'},
  {path:'vendor',loadChildren:'./vendor/vendor.module#VendorModule'},
  {path:'',redirectTo:'Login',pathMatch:'full'}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
