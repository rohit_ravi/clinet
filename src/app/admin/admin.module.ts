import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { HomeComponent } from './home/home.component';
import { CreateVendorComponent } from './create-vendor/create-vendor.component';
import { VendorsListComponent } from './vendors-list/vendors-list.component';
import { MenuComponent } from './menu/menu.component';


@NgModule({
  declarations: [AdminComponent, HomeComponent, CreateVendorComponent, VendorsListComponent, MenuComponent],
  imports: [
    CommonModule,
    AdminRoutingModule
  ]
})
export class AdminModule { }
