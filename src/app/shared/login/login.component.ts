import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ServiceService } from '../services/service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public Pmessage;
  form = new FormGroup({
    "username":new FormControl("",Validators.required),
    "password":new FormControl("",Validators.required)
  })
  constructor(private router:Router,private Service:ServiceService) { 

  }
  get username(){
     return this.form.get("username")
  }

  ngOnInit() {
   
  }
  onSubmit(e){
    

}

}

