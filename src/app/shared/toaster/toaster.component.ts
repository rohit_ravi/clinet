import { Component, OnInit,Input,OnChanges} from '@angular/core';


@Component({
  selector: 'app-toaster',
  templateUrl: './toaster.component.html',
  styleUrls: ['./toaster.component.css']
})
export class ToasterComponent implements OnInit,OnChanges {
@Input() public message:string;
public Alert;
  constructor() {
  
   }
ngOnChanges(change){
  if(change.message.currentValue!=undefined){
this.Alert=change.message.currentValue;
    setTimeout(()=>{this.Alert=''},3000)
  }

}
  ngOnInit() {
  }
 
}
