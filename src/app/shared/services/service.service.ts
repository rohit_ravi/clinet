import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(private http: HttpClient) {

   }
  Login(Obj){
    return this.http.post('http://localhost:2000/login/login-check', Obj);
  }
}
